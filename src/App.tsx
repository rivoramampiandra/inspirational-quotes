import {useEffect, useState} from "react";

import quotesData from './assets/quotes.json';

import './App.css'

interface QuoteInterface {
    quote: string,
    author: string
}
function App() {
    const [quotes, setQuotes] = useState<QuoteInterface[]>([]);

    const [index, setIndex] = useState<number>(0);

    useEffect((): void => {
        const randomIndex: number = Math.floor(Math.random() * quotesData.length);

        setIndex(randomIndex);
        setQuotes(quotesData);
    }, []);


    const handleNext = (): void => {
        setIndex((prevIndex: number) => (prevIndex + 1) % quotes.length);
    };

    const handlePrev = (): void => {
        setIndex((prevIndex: number) => (prevIndex - 1 + quotes.length) % quotes.length);
    };

    return (
        <>
            <div className="carousel-container">
                <div className="quote-container">
                    <p>"{quotes[index]?.quote}"</p>
                    <footer> - {quotes[index]?.author}</footer>
                </div>
                <div className="buttons-container">
                    <button className="prev-btn" onClick={handlePrev}>Previous</button>
                    <button className="next-btn" onClick={handleNext}>Next</button>
                </div>
            </div>
        </>
    )
}

export default App;
